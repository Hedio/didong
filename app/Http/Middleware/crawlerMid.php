<?php

namespace App\Http\Middleware;

use Closure;

class crawlerMid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    private $token = "Hiep15520215";

    public function checkAuthCrawler($token){
        if($token!=$this->token){
            abort(404);
        }
    }


    public function handle($request, Closure $next)
    {
        $this->checkAuthCrawler($request->token);
        
        return $next($request);
    }
}
