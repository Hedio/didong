<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;


use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Mail;
use App\Mail\ApiAuth;
use App\Mail\ApiResetPW;

use App\User;


use App\Category;
use App\Comic;
use App\CategoryDetails;
use App\ComicImgs;

use App\Like;
use App\Save;
use App\Comment;

function generateRandomToken($length = 15)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

class APIController extends Controller
{
    public function __construct()
    {

    }

    public function CheckUser($email,$password){
        $password = md5($password);

        $user = User::where([
            'email'=>$email,
            'password'=>$password,
            'ISACTIVED'=>1
        ])->first();

        return $user;
    }

    public function postCheckAuth(Request $request){
        $email = $request->email;
        $password = $request->password;
        $user = $this->CheckUser($email,$password);

        if($user){
            return $user;
        }
        abort(404);

        
    }

    public function postRegister(Request $request){
        $email = $request->email;
        $password = $request->password;
        $name = $request->name;

        if(User::where("email",$email)->orWhere("name",$name)->count()!=0){
            return 404;
        }

        $user = new User;
        $user->email = $email;
        $user->password = md5($password);
        $user->name = $name;

        $token = generateRandomToken(100);
        $user->remember_token = $token;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();
 
        $params = "/api/mobile-call/confirmRegis?id=".$user->id . "&token=" .$token;
        Mail::to($email)->send(new ApiAuth("no-reply@didongProject.com", "registeration", $params));

        return $user;

    }
    public function confirmRegis(Request $request){
        $id = $request->id;
        $token = $request->token;
        $user = User::find($id);
        if($user->remember_token==$token){
            $user->ISACTIVED = 1;
            $user->email_verified_at = Carbon::now();
            $user->save();
            return "Registration complete";
        }
        return "Failed";
    }

    public function postResetPW(Request $request){
        $email = $request->email;
        $user = User::where([
            "email"=>$email,
            "ISACTIVED"=>1
        ])->first();
        if($user){
            $token = generateRandomToken(10);
            $user->remember_token = $token;
            $user->save();

            $params = "/api/mobile-call/ConfirmResetToken?id=".$user->id . "&token=" .$token;
            Mail::to($email)->send(new ApiResetPW("no-reply@didongProject.com", "Reset Password", $token));
            return;
        }
        abort(404);
    }

    public function ConfirmResetToken(Request $request){
        $id = $request->user_id;
        $token = $request->token;
        $user = User::find($id);
        if($user->remember_token==$token){
            return;
        }
        abort(404);

    }

    public function postChangePW(Request $request){
        $id = $request->user_id;
        $password = $request->password;
        $token = $request->token;

        $user = User::where([
            "id"=>$id,
            "remember_token"=>$token
        ])->first();

        if($user){
            $user->password = md5($password);
            $user->save();
            return;
        }
        abort(404);

    }

    public function AddComicsInfo(&$comics){
        foreach($comics as &$comic){
            $likes = Like::where("comic_novel_id",$comic["id"])->count();
            $saves = Save::where("comic_novel_id",$comic["id"])->count();
            $comments = Comment::where("comic_novel_id",$comic["id"])->count();
            //->join("user","user.id","=","comments.user_id")


            $comic["likes"] = $likes;
            $comic["saves"] = $saves;
            $comic["comments"] = $comments;
        }
        unset($comic);
    }

    


    public function addCategories(Request $request){
        $data = $request->data;
        foreach($data as $d){
            $cate = Category::where("name",$d)->first();
            if(!$cate){
                $cate = new Category();
                $cate->name = $d;
                $cate->save();
            }
        }

    }

    public function addComics(Request $request){
        $data = $request->data;
        foreach($data as $d){
            if(Comic::where("comic_crawl_id",$d["comic_crawl_id"])->count()==0){
                $comic = new Comic();
                $comic->original_url = $d["original_url"];
                $comic->comic_crawl_id = $d["comic_crawl_id"];
                $comic->thumb = $d["thumb"];
                $comic->title = $d["title"];
                $comic->save();
            }
        }

    }

    public function addComicInfo(Request $request){
        $d = $request->data;
        if(Comic::where("comic_crawl_id",$d["comic_crawl_id"])->value("short_intro")==""){
            Comic::where("comic_crawl_id",$d["comic_crawl_id"])->update([
                "current_chapters"=>$d["current_chapters"],
                "short_intro"=>$d["short_intro"]             
            ]);

            foreach($d["categogies"] as $cate){
                $CategoryDetails = new CategoryDetails();
                $comic_novel_id = Comic::where("comic_crawl_id",$d["comic_crawl_id"])->value("id");
                $category_id = Category::whereRaw("LOWER(`name`) LIKE ?",[strtolower($cate)])->value("id");

                if($comic_novel_id && $category_id){
                    $CategoryDetails->comic_novel_id=$comic_novel_id;
                    $CategoryDetails->category_id=$category_id;
                    $CategoryDetails->save();
                }
                
            }  
          

        }else{
            return "continue";
        }

    }

    public function addComicImgs(Request $request){
        $d = $request->data;
        foreach($d["imgs"] as $img){
            $ComicImgs = new ComicImgs();
            $ComicImgs->comic_id = $d["comic_crawl_id"];
            $ComicImgs->chapter_index = $d["chapter_index"];
            $ComicImgs->url = $img;
            $ComicImgs->save();
        }

    }

    public function getCategories(Request $request){     
        return Category::select("name","id")->get()->toArray(); 
    }

    public function getCarousel(Request $request){
        $take = $request->take;
        return Comic::orderBy("views","desc")->take($take)->get()->toArray();
    }

    public function getComics(Request $request){
        $cate_id= $request->cate_id;
        $skip = $request->skip;
        $take = $request->take;

        if($cate_id==-1){
            $first_cate = Category::first();
            $comics =  CategoryDetails::where("category_details.category_id",$first_cate->id)->join("comic_novel","comic_novel.id","=","category_details.comic_novel_id")->select("comic_novel.*")->skip($skip)->take($take)->get()->toArray();

        }else{
            $comics = CategoryDetails::where("category_details.category_id",$cate_id)->join("comic_novel","comic_novel.id","=","category_details.comic_novel_id")->select("comic_novel.*")->skip($skip)->take($take)->get()->toArray();
        }

        $this->AddComicsInfo($comics);

        /*foreach($comics as &$comic){
            $likes = Like::where("comic_novel_id",$comic["id"])->count();
            $saves = Save::where("comic_novel_id",$comic["id"])->count();
            $comments = Comment::where("comic_novel_id",$comic["id"])->count();
            //->join("user","user.id","=","comments.user_id")


            $comic["likes"] = $likes;
            $comic["saves"] = $saves;
            $comic["comments"] = $comments;

        }*/

        return $comics;

    }

    public function getComicDetails(Request $request){ 
        $comic_id = $request->comic_id;
        $comic = Comic::where("id",$comic_id)->first();


        $likes = Like::where("comic_novel_id",$comic->id)->count();
        $saves = Save::where("comic_novel_id",$comic->id)->count();
        //$comments = Comment::where("comic_novel_id",$comic["id"])->count();
        $comments = Comment::where("comments.comic_novel_id",$comic->id)->join("users","users.id","=","comments.user_id")->select("users.name as username", "comments.content")->get()->toArray();


        $comic["likes"] = $likes;
        $comic["saves"] = $saves;
        $comic["comments"] = $comments;

        
        //$comic["comment_contents"] = $comment_contents;

        

        return $comic;
    }

    public function getRelatedComics(Request $request){
        $comic_id = $request->comic_id;
        $take = $request->take;

        $comic = Comic::where("id",$comic_id)->first();

        //related comics
        $cate_id = CategoryDetails::where("category_details.comic_novel_id",$comic->id)->value("category_id");
        $related_comics = CategoryDetails::where("category_details.category_id",$cate_id)->join("comic_novel","comic_novel.id","=","category_details.comic_novel_id")->select("comic_novel.*")->take($take)->get()->toArray();

        foreach($related_comics as &$comic){
            $likes = Like::where("comic_novel_id",$comic["id"])->count();
            $saves = Save::where("comic_novel_id",$comic["id"])->count();
            $comments = Comment::where("comic_novel_id",$comic["id"])->count();
            //->join("user","user.id","=","comments.user_id")


            $comic["likes"] = $likes;
            $comic["saves"] = $saves;
            $comic["comments"] = $comments;

        }

        return $related_comics;
    }

    public function getComicImgs(Request $request){
        $comic_crawl_id = $request->comic_crawl_id;
        $chapter_index = $request->chapter_index;
        return ComicImgs::where([
            "comic_id"=>$comic_crawl_id,
            "chapter_index"=>$chapter_index
        ])->get()->toArray();

    }


    public function postView(Request $request){
        $comic_id = $request->comic_id;
        Comic::where("id",$comic_id)->increment("views");
    }

    public function postLike(Request $request){
        if(Like::where([
            "comic_id"=>$request->comic_id,
            "user_id"=>$request->user_id
        ])->count()==0){
            $like = new Like();
            $like->comic_id = $request->comic_id;
            $like->user_id = $request->user_id;
            $like->save();
        }
    }

    public function postSave(Request $request){
        if(Save::where([
            "comic_novel_id"=>$request->comic_novel_id,
            "user_id"=>$request->user_id
        ])->count()==0){
            $save = new Save();
            $save->comic_novel_id = $request->comic_novel_id;
            $save->user_id = $request->user_id;
            $save->save();
        }
        
    }

    public function getSaves(Request $request){
        $comics = Save::where([
            "save.user_id"=>$request->user_id,         
        ])->join("comic_novel","comic_novel.id","=","save.comic_novel_id")->select("comic_novel.*")->get()->toArray();

        $this->AddComicsInfo($comics);
        return $comics;
    }

    public function postComment(Request $request){
        $Comment = new Comment();
        $Comment->comic_novel_id = $request->comic_novel_id;
        $Comment->user_id = $request->user_id;
        $Comment->content = $request->content;
        $Comment->save();
    }





}
