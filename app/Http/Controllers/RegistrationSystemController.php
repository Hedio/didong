<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use App\User;
use App\Admin;
use Hash;
use Sentinel;
use Reminder;
use Mail;
use Activation;
class RegistrationSystemController extends Controller
{
	// login page
	public function GetLogin(){
		if(Auth('admin')->check()){
			return redirect('home/');
		}else{
			return view('registration_system.login');
		}
	}
	public function Login(Request $request){
		if(Auth('admin')->check()){
			return redirect('home/');
		}else{
			$user = [
						'email'=>$request->email,
						'password'=>$request->password,
					];
			if(Auth::guard('admin')->attempt($user)){
				return redirect('home/');
			}else{
				return redirect()->back()->with(['error'=>'thông tin tài khoảng hoặc mật khẩu chưa chính xác.' , 'BackEmail' => $request->email ]);
			}
		}
	}
	public function GetForgotPassword(){
		if(Auth::check()){
			return redirect('home/');
		}else{
			return view('registration_system.forgot_password');
		}
	}
	// logout
	public function Logout(){
		Auth::logout();
		return redirect('registration/login');
	}
	
}
