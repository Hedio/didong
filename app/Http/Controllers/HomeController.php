<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\CanBeBought;
use Auth;
use Session;
use App\User;
use App\Admin;
use Hash;
use Sentinel;
use Reminder;
use Activation;


class HomeController extends Controller
{

    public function GetIndex(){
    	if(Auth('admin')->check()){
        	return view('page.index');
    	}else{
    		return view('registration_system.login');
    	}
    }
    // get page manage admin
    public function GetManageAdmin(){
    	if(Auth('admin')->check()){
    		return view('page.admin');
    	}else{
    		return view('registration_system.login');
    	}
    }
    // get page manage user
    public function GetManageUser(){
    	if(Auth('admin')->check()){
    		return view('page.user');
    	}else{
    		return view('registration_system.login');
    	}
    }
    // get page categories
    public function GetCategories(){
    	if(Auth('admin')->check()){
    		return view('page.categories');
    	}else{
    		return view('registration_system.login');
    	}
    }
    // get page comment
    public function GetComment(){
    	if(Auth('admin')->check()){
    		return view('page.comment');
    	}else{
    		return view('registration_system.login');
    	}
    }
    // get page post
    public function GetPost(){
    	if(Auth('admin')->check()){
    		return view('page.post');
    	}else{
    		return view('registration_system.login');
    	}
    }
    // get page control
    public function GetControl(){
    	if(Auth('admin')->check()){
    		return view('page.control');
    	}else{
    		return view('registration_system.login');
    	}
    }
}