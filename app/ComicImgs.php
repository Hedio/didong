<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComicImgs extends Model
{
    protected $table = "imgs";
	public $timestamps = false;
}
