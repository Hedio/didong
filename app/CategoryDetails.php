<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDetails extends Model
{
    protected $table = "category_details";
	public $timestamps = false;
}
