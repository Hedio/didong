<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApiAuth extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $_from;
    public $_subject;
    public $_content;


    public function __construct($from,$subject,$content)
    {
        $this->_from = $from;
        $this->_subject = $subject;
        $this->_content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->_from)->subject($this->_subject)->view('MailTemplate.ApiAuth',[
            "_content"=>$this->_content

        ]);
    }
}
