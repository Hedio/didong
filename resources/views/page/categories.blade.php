@extends('page.nav_footer')
@section('title','Categories')
@section('content')
  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Catelogories</h3>
        </div>
      </div>

      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Danh sách categories</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-buttons" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Họ tên</th>
                    <th>Email</th>
                    <th>Số điện thoại</th>
                    <th>Trạng thái</th>
                    <th>Địa chỉ</th>
                    <th>Ngày khởi tạo</th>
                    <th>Ngày cập nhật</th>
                    <th>Edit</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
  </div>
</div>
<!-- delete modal -->
<div id="DeleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Xóa</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <form data-parsley-validate class="form-horizontal form-label-left" action="" method="post">
            <div class="modal-body">
              <div id="testmodal" style="padding: 5px 20px;">
                <h5 style="font-size: 17px;">
                Bạn có chắt mình muốn xóa mục này? Nội dung được xóa sẽ không thể hoàn tác.</h5>
              </div>
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
              <input type="hidden" name="table" value="users">
              <input type="hidden" name="id" id="id">
            </div>
            <div class="x_content">
            <br />
              <div class="ln_solid"></div>
              <div class="form-group">
                <div >
                  <button class="btn btn-primary" type="button">Hủy</button>
                  <button type="submit" class="btn btn-success">Xóa</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop()