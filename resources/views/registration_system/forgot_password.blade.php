@extends('registration_system.nav_footer')
@section('title','Quên mật khẩu')
@section('content')
<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Khôi phục mật khẩu</div>
      <div class="card-body">
        <div class="text-center mb-4">
            @if(session('success'))
              <h4>Quá trình khôi phục sắp hoàn thành.</h4>
              <p>{{session("success")}}</p>
            @else
              <h4>Bạn đã quên mật khẩu?</h4>
              <p>Nhập địa chỉ email của bạn, chúng tôi sẽ gửi thông tin khôi phục mật khẩu đến địa chỉ email của bạn.</p>
            @endif
        </div>
        <form method="post">
          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          {{csrf_field()}}  
          <div class="form-group">
            <div class="form-label-group">
              <input type="email" id="inputEmail" class="form-control" placeholder="Enter email address" required="required" autofocus="autofocus" name="email">
              <label for="inputEmail">Email</label>
            </div>
          </div>
          <input type="submit" name="forgot_password_btn" class="btn btn-primary btn-block" value="Đặt lại mật khẩu">
        </form>
        <div class="text-center">
          <a class="d-block small" href="{{route('Login')}}">Đăng nhập</a>
        </div>
        @if(session('error'))
          <div class="alert alert-danger">
              <strong>Khôi phục thất bại!</strong>
              </br>
              <p>{{session("error")}}</p>
          </div>  
        @endif
      </div>
    </div>
  </div>
@stop()