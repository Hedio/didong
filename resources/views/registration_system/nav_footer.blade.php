<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin - @yield('title')</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="{{asset('/public/asset/registration-system-css/sb-admin.css')}}" rel="stylesheet">
</head>
<body class="bg-dark" id="page-top">
	@yield('content');
</body>
</html>