@extends('registration_system.nav_footer')
@section('title','Đăng nhập')
@section('content')
<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Đăng nhập</div>
      <div class="card-body">
        @if(session('error'))
          <div class="alert alert-danger">
              <strong>Đăng nhập thất bại!</strong>
              </br>
              <marquee>{{session("error")}}</marquee>
          </div>  
        @endif
        <form action="{{Route('PostLogin')}}" method="post">
          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          <div class="form-group">
            <div class="form-label-group">
              <input type="email" id="inputEmail" class="form-control" required autofocus="autofocus" placeholder="Email" name="email"
                @if(session('BackEmail'))
                  value="{{session("BackEmail")}}" 
                @endif
              >
              <label for="inputEmail">Email</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password" >
              <label for="inputPassword">Password</label>
            </div>
          </div>
          <div class="form-group">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="remember-me">
                Nhớ mật khẩu
              </label>
            </div>
          </div>
          <input type="submit" name="login_btn" class="btn btn-primary btn-block" value="Đăng nhập">
           {{csrf_field()}}
        </form>
        <div class="text-center">
          <a class="d-block small" href="{{route('ResetPassword')}}">Quên mật khẩu?</a>
        </div>
      </div>
    </div>
  </div>
@stop()