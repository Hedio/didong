<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'crawler-calls','middleware'=>'crawlerMid'], function() {
    Route::post('addCategories', 'APIController@addCategories');
    Route::post('addComics', 'APIController@addComics');
    Route::post('addComicInfo', 'APIController@addComicInfo');
    Route::post('addComicImgs', 'APIController@addComicImgs');
});

Route::group(['prefix'=>'mobile-call'], function() {
    Route::post('getCategories', 'APIController@getCategories');
    Route::post('getCarousel', 'APIController@getCarousel');

    Route::post('getComics', 'APIController@getComics');
    Route::post('getComicDetails', 'APIController@getComicDetails');
    Route::post('getRelatedComics', 'APIController@getRelatedComics');

    Route::post('getComicImgs', 'APIController@getComicImgs');

    Route::post('postView', 'APIController@postView');
    Route::post('postLike', 'APIController@postLike');
    Route::post('postSave', 'APIController@postSave');
    Route::post('getSaves', 'APIController@getSaves');
    Route::post('postComment', 'APIController@postComment');


    Route::post('postCheckAuth', 'APIController@postCheckAuth');

    Route::post('postRegister', 'APIController@postRegister');
    Route::get('confirmRegis', 'APIController@confirmRegis');

    Route::post('postResetPW', 'APIController@postResetPW');
    Route::post('ConfirmResetToken', 'APIController@ConfirmResetToken');
    Route::post('postChangePW', 'APIController@postChangePW');

});