<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix'=>'registration'],function(){
	// // logins
	Route::get('login','RegistrationSystemController@GetLogin')->name('Login');
	Route::post('login','RegistrationSystemController@Login')->name('PostLogin');
	// // // loguot
	Route::get('logout','RegistrationSystemController@Logout')->name('UserLogout');
	// // // signup
	// Route::get('signup','RegistrationSystemController@GetSignUp')->name('UserSignUp');
	// Route::post('signup','RegistrationSystemController@SignUp')->name('UserPostSignUp');
	// // // forgot
	Route::get('forgot_password','RegistrationSystemController@GetForgotPassword')->name('ResetPassword');
	// Route::post('forgot_password','RegistrationSystemController@ForgotPassword')->name('UserResetPassword');
	// // // reset qua mail
	// Route::get('change_forgot_form/{email}',function($email){
	// 	return view('web_page.registration_system.reset_password',compact('email'));
	// })->name('change_forgot_form');
	// // //change pass
	// Route::get('change_password','RegistrationSystemController@GetChangePassword')->name('UserChangePassword');
	// Route::post('change_password','RegistrationSystemController@ChangePassword')->name('UserPostChangePassword');
	// // //profile chưa làm
	// Route::get('profile','RegistrationSystemController@GetProfile')->name('UserProfile');
});
Route::group(['prefix'=>'home'],function(){
	Route::get('/','HomeController@GetIndex')->name('Home');
	Route::get('admin','HomeController@GetManageAdmin')->name('Admin');
	Route::get('user','HomeController@GetManageUser')->name('User');
	Route::get('categories','HomeController@GetCategories')->name('Categories');
	Route::get('comment','HomeController@GetComment')->name('Comment');
	Route::get('post','HomeController@GetPost')->name('Post');
	Route::get('control','HomeController@GetControl')->name('Control');
});