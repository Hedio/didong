$('#ActiveModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var id = button.data('id') 
        var modal = $(this)
        modal.find('.modal-footer input').val(id);
    })
$('#DeleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var id = button.data('id') 
        var modal = $(this)
        modal.find('.modal-footer input').val(id);
    })
$('#RepMessageModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var id = button.data('id')
        var fullname = button.data('fullname')
        var email = button.data('email')
        var message_content = button.data('message_content') 
        var modal = $(this)
        modal.find('.modal-body #id').val(id);
        modal.find('.modal-body #email').val(email);
        modal.find('.modal-body #fullname').val(fullname);
        modal.find('.modal-body #message_content').val(message_content);
        document.getElementById("ShowEmail").innerHTML = button.data('email');
    })
window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 4000);
// auto show modal when reload page
    // @if(session('success'))
    //     <script>
    //       $(document).ready(function(){
    //         $('#SuccessModal').modal('show');
    //       });
    //     </script>
    // @endif