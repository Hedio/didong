<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComicNovelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comic_novel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('comic_crawl_id')->nullable();
            $table->longtext('original_url')->nullable();
            $table->longtext('thumb')->nullable();
            $table->text('short_intro')->nullable();
            $table->string('title')->nullable();
            $table->boolean('isComic')->default(1);
            $table->longtext('content')->nullable();
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('current_chapters')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comic_novel');
    }
}
